#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./run

# begin to execute the nvprof metrics 

sudo nvprof -m    stall_inst_fetch    ./run
sudo nvprof -m    stall_texture   ./run


end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
