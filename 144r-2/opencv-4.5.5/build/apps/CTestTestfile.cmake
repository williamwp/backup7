# CMake generated Testfile for 
# Source directory: /home/wangpeng/backup7/144r-2/opencv-4.5.5/apps
# Build directory: /home/wangpeng/backup7/144r-2/opencv-4.5.5/build/apps
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("annotation")
subdirs("visualisation")
subdirs("interactive-calibration")
subdirs("version")
subdirs("model-diagnostics")
