#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./run

# begin to execute the nvprof metrics 

if false; then
sudo nvprof -m              l2_read_transactions  $ELF
sudo nvprof -m             l2_write_transactions  $ELF
sudo nvprof -m            dram_read_transactions  $ELF
sudo nvprof -m           dram_write_transactions  $ELF
sudo nvprof -m                   global_hit_rate  $ELF
sudo nvprof -m                    local_hit_rate  $ELF
sudo nvprof -m          gld_requested_throughput  $ELF
sudo nvprof -m          gst_requested_throughput  $ELF
sudo nvprof -m                    gld_throughput  $ELF
sudo nvprof -m                    gst_throughput  $ELF
sudo nvprof -m             local_memory_overhead  $ELF
sudo nvprof -m                tex_cache_hit_rate  $ELF
sudo nvprof -m              l2_tex_read_hit_rate  $ELF
sudo nvprof -m             l2_tex_write_hit_rate  $ELF
sudo nvprof -m              dram_read_throughput  $ELF
sudo nvprof -m             dram_write_throughput  $ELF
sudo nvprof -m              tex_cache_throughput  $ELF
sudo nvprof -m            l2_tex_read_throughput  $ELF
sudo nvprof -m           l2_tex_write_throughput  $ELF
sudo nvprof -m                l2_read_throughput  $ELF
sudo nvprof -m               l2_write_throughput  $ELF
sudo nvprof -m            sysmem_read_throughput  $ELF
sudo nvprof -m           sysmem_write_throughput  $ELF
sudo nvprof -m             local_load_throughput  $ELF
sudo nvprof -m            local_store_throughput  $ELF
fi

sudo nvprof -m            shared_load_throughput  $ELF
sudo nvprof -m           shared_store_throughput  $ELF
sudo nvprof -m                    gld_efficiency  $ELF
sudo nvprof -m                    gst_efficiency  $ELF
sudo nvprof -m            tex_cache_transactions  $ELF


end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
