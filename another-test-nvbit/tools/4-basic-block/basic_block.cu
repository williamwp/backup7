#include <assert.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <map>

/* every tool needs to include this once */
#include "nvbit_tool.h"

/* nvbit interface file */
#include "nvbit.h"

/* provide some __device__ functions */
#include "utils/utils.h"

/* kernel id counter, maintained in system memory */
uint32_t kernel_id = 0;

/* total instruction counter, maintained in system memory, incremented by
 * "counter" every time a kernel completes  */
uint64_t total_application_instr = 0;

/* kernel instruction counter, updated by the GPU threads */
__managed__ uint64_t counter = 0;

/* pointer to memory location containing BBVs */
__managed__ int *basic_block_value;

// Total threads of the kernel being launched
unsigned int tot_threads = 0;

// Unique kernel ID
unsigned int kid = 0;

// Total number of basic blocks to keep track of
__managed__ unsigned int basic_blocks = 0;
std::map <std::string, int> kernal_basic_block_map;

/* global control variables for this tool */
uint32_t ker_begin_interval = 0;
uint32_t ker_end_interval = UINT32_MAX;
int verbose = 1;
int count_warp_level = 1;
int exclude_pred_off = 0;

/* a pthread mutex, used to prevent multiple kernels to run concurrently and
 * therefore to "corrupt" the counter variable */
pthread_mutex_t mutex;

/* instrumentation function that we want to inject, please note the use of
 * 1. "extern "C" __device__ __noinline__" to prevent code elimination by the
 * compiler.
 * 2. NVBIT_EXPORT_FUNC(count_instrs) to notify nvbit the name of the function
 * we want to inject. This name must match exactly the function name */
extern "C" __device__ __noinline__ void count_instrs(int num_instrs,
                                                     int count_warp_level,
                                                     int bb) {
    // Get the global warp id to update the basic_block_value
    int global_wid = get_global_warp_id();
    /* all the active threads will compute the active mask */
    const int active_mask = __ballot(1);
    /* each thread will get a lane id (get_lane_id is in utils/utils.h) */
    const int laneid = get_laneid();
    /* get the id of the first active thread */
    const int first_laneid = __ffs(active_mask) - 1;
    /* count all the active thread */
    const int num_threads = __popc(active_mask);
    /* only the first active thread will perform the atomic */
    if (first_laneid == laneid) {
        // Index based upon the bb param
        basic_block_value[global_wid * basic_blocks + bb] += num_threads;
        /*
        if (count_warp_level) {
            atomicAdd((unsigned long long *)&counter, 1 * num_instrs);
        } else {
            atomicAdd((unsigned long long *)&counter, num_threads * num_instrs);
        }
        */
    }
}
NVBIT_EXPORT_FUNC(count_instrs);

extern "C" __device__ __noinline__ void count_pred_off(int predicate,
                                                       int count_warp_level) {
    const int active_mask = __ballot(1);

    const int laneid = get_laneid();

    const int first_laneid = __ffs(active_mask) - 1;

    const int predicate_mask = __ballot(predicate);

    const int mask_off = active_mask ^ predicate_mask;

    const int num_threads_off = __popc(mask_off);
    if (first_laneid == laneid) {
        if (count_warp_level) {
            /* if the predicate mask was off we reduce the count of 1 */
            if (predicate_mask == 0)
                atomicAdd((unsigned long long *)&counter, (unsigned int)-1);
        } else {
            atomicAdd((unsigned long long *)&counter, -num_threads_off);
        }
    }
}
NVBIT_EXPORT_FUNC(count_pred_off)

/* nvbit_at_init() is executed as soon as the nvbit tool is loaded. We
 * typically do initializations in this call. In this case for instance we get
 * some environment variables values which we use as input arguments to the tool
 */
void nvbit_at_init() {
    /* just make sure all managed variables are allocated on GPU */
    setenv("CUDA_MANAGED_FORCE_DEVICE_ALLOC", "1", 1);

    /* we get some environment variables that are going to be use to selectively
     * instrument (within a interval of kernel indexes and instructions). By
     * default we instrument everything. */
    GET_VAR_INT(ker_begin_interval, "KERNEL_BEGIN", 0,
                "Beginning of the kernel launch interval where to apply "
                "instrumentation");
    GET_VAR_INT(
        ker_end_interval, "KERNEL_END", UINT32_MAX,
        "End of the kernel launch interval where to apply instrumentation");
    GET_VAR_INT(count_warp_level, "COUNT_WARP_LEVEL", 1,
                "Count warp level or thread level instructions");
    GET_VAR_INT(exclude_pred_off, "EXCLUDE_PRED_OFF", 0,
                "Exclude predicated off instruction from count");
    GET_VAR_INT(verbose, "TOOL_VERBOSE", 0, "Enable verbosity inside the tool");
    std::string pad(100, '-');
    printf("%s\n", pad.c_str());
}

/* nvbit_at_function_first_load() is executed every time a function is loaded
 * for the first time. Inside this call-back we typically get the vector of SASS
 * instructions composing the loaded CUfunction. We can iterate on this vector
 * and insert call to instrumentation functions before or after each one of
 * them. */
void nvbit_at_function_first_load(CUcontext ctx, CUfunction func) {
    /* Get the static control flow graph of instruction */
    const CFG_t &cfg = nvbit_get_CFG(ctx, func);
    if (cfg.is_degenerate) {
        printf(
            "Warning: Function %s is degenerated, we can't compute basic "
            "blocks statically",
            nvbit_get_func_name(ctx, func));
    }

    if (verbose) {
        printf("Function %s\n", nvbit_get_func_name(ctx, func));
        int cnt = 0;
        for (auto &bb : cfg.bbs) {
            printf("Basic block id %d - num instructions %ld\n", cnt++,
                   bb->instrs.size());
            for (auto &i : bb->instrs) {
                i->print(" ");
            }
        }
    }

    if (1) {
        printf("inspecting %s - number basic blocks %ld\n",
               nvbit_get_func_name(ctx, func), cfg.bbs.size());
    }

    int local_bb = 0;
    for (auto &bb : cfg.bbs) {
        Instr *i = bb->instrs[0];
        nvbit_insert_call(i, "count_instrs", IPOINT_BEFORE);
        nvbit_add_call_arg_const_val32(i, bb->instrs.size());
        nvbit_add_call_arg_const_val32(i, count_warp_level);
        nvbit_add_call_arg_const_val32(i, local_bb++);
        if (verbose) {
            i->print("Inject count_instr before - ");
        }
    }

    kernal_basic_block_map.insert(std::pair<std::string,int>(nvbit_get_func_name(ctx, func), cfg.bbs.size())); 
    int *bbs;
    basic_blocks = cfg.bbs.size();
    cudaMallocManaged(&bbs, (tot_threads / 32) * (basic_blocks) * sizeof(int));

    basic_block_value = bbs;
    for(unsigned int i = 0; i < (tot_threads / 32) * (basic_blocks); i++){
        basic_block_value[i] = 0;
    }

    if (exclude_pred_off) {
        /* iterate on instructions */
        for (auto i : nvbit_get_instrs(ctx, func)) {
            /* inject only if instruction has predicate */
            if (i->hasPred()) {
                /* inject function */
                nvbit_insert_call(i, "count_pred_off", IPOINT_BEFORE);
                /* add predicate as argument */
                nvbit_add_call_arg_pred_val(i);
                /* add count warp level option */
                nvbit_add_call_arg_const_val32(i, count_warp_level);
                if (verbose) {
                    i->print("Inject count_instr before - ");
                }
            }
        }
    }
}

void nvbit_at_cuda_event(CUcontext ctx, int is_exit, nvbit_api_cuda_t cbid,
                         const char *name, void *params, CUresult *pStatus) {
    /* Identify all the possible CUDA launch events */
    if (cbid == API_CUDA_cuLaunch || cbid == API_CUDA_cuLaunchKernel_ptsz ||
        cbid == API_CUDA_cuLaunchGrid || cbid == API_CUDA_cuLaunchGridAsync ||
        cbid == API_CUDA_cuLaunchKernel) {
        
	cuLaunch_params *p = (cuLaunch_params *)params;
          
        if (!is_exit) {

            pthread_mutex_lock(&mutex);

            // Get the launch parameters to we can narrowly allocate memory
            cuLaunchKernel_params_st *p_test = (cuLaunchKernel_params_st *)params;

            // Only look at 2D kernels (common case, might change if 3D ones exist)
            unsigned int gx = p_test->gridDimX;
            unsigned int gy = p_test->gridDimY;
            unsigned int bx = p_test->blockDimX;
            unsigned int by = p_test->blockDimY;

            // Set global number of threads
            tot_threads = gx * gy * bx * by;
           
            if (kernel_id >= ker_begin_interval &&
                kernel_id < ker_end_interval) {
                nvbit_enable_instrumented(ctx, p->f, true);
            } else {
                nvbit_enable_instrumented(ctx, p->f, false);
            }

        } else {
           
	    pthread_mutex_unlock(&mutex);
        }
    }
}

