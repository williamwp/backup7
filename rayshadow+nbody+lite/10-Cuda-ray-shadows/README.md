# 1 Introduction
This code is an implementation of a CUDA-accelerated ray tracing renderer. Ray tracing is a computationally intensive rendering algorithm used to generate realistic images. The code utilizes the CUDA programming model to leverage the parallel computing power of the GPU for accelerated rendering.

The code begins by defining the window size (nx and ny) and the number of samples per pixel (ns). It then allocates the frame buffer (fb) to store the rendering results using the cudaMallocManaged function, which allocates memory on both the host and GPU device.

Next, the code defines the thread block size (tx and ty) and the number of thread blocks (blocks). It also creates an array of curandState structures (d_rand_state) for generating random numbers.

Then, the code creates the objects (hitable) and the camera (camera) in the scene on the GPU. The data for the objects and camera are stored in device memory.

The code enters the main loop, where in each rendering step, it first initializes the camera parameters and then calls the rendering function (render) for ray tracing to compute the color value for each pixel and stores the results in the frame buffer. After rendering, the camera resources are released.

Finally, the code cleans up memory and resets the CUDA device.

This code implements a basic ray tracing algorithm and utilizes the CUDA programming model and GPU parallel computing power for efficient rendering. It demonstrates how to leverage CUDA for accelerating computationally intensive tasks and can be extended and optimized by adjusting parameters and adding more features to the rendering process.  

# 2 Command 
$ ./a.out 




