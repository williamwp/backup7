#include <stdio.h>

__global__ void hello() {
    printf("Hello world from device 1 \n");
    
}

int main() {
    hello<<<1, 1>>>(); 
    // call 
    printf("Hello world from host 1 \n");

    printf("Hello world from host 2 \n");

    printf("Hello world from host 3 \n");

    return 0;
}
