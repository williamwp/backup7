#include <stdio.h>

__global__ void hello() {
    printf("Hello world from device 1 \n");
    
}

int add(int &a, int &b)
{
    return a+b;
}

int main() {
    
    int c = 1;
    int d = 2;
    int e = add(c,d);

    printf("e = %d \n", e);

    hello<<<1, 1>>>();
    printf("Hello world from host 1 \n");

    printf("Hello world from host 2 \n");

    printf("Hello world from host 3 \n");

    return 0;
}
