#include <stdio.h>

__global__ void hello1() {
    printf("Hello world from device 1 \n");
    
}

__global__ void hello2() (
    printf("hello world from device2 \n");
}

int main() {
    hello1<<<1, 1>>>();
    hello2<<<1, 2>>>();
    printf("Hello world from host 1 \n");

//    printf("Hello world from host 2 \n");

  //  printf("Hello world from host 3 \n");

    return 0;
 }
