iii


/* counters for unique cache lines accessed and for
the number of memory instructions executed */ 
__managed__ float uniq_lines = 0; 
__managed__ long mem_instrs = 0; 
extern "C" __device__ __noinline__
void ifunc(int pred, int r1, int r2, int imm) {
/* Return if predicate is false */ 
if(!pred) return;

 /* Construct address */
long addr = (((long)r1) | ((long)r2 << 32)) + imm;

/* Compute active mask of the warp */
int mask = __ballot(1);

/* Only the first active thread in the warp
increments the memory instruction counter */
if (get_lane_id() == __ffs(mask) - 1)
atomicAdd(&mem_instrs, 1);

/* Count how many threads in the warp access
the same cache line */
long cache_addr = addr >> LOG2_CACHE_LINE_SIZE;
int cnt = __popc(__match_any_sync(mask, cache_addr));

/* Each thread contributes proportionally to the
cache line counter */
atomicAdd(&uniq_lines, 1.0f / cnt);
} NVBIT_EXPORT_DEV_FUNC(ifunc);

void nvbit_at_cuda_driver_call(CUcontext ctx,
int is_exit, cbid_t cbid, const char *name,
void *params, CUresult *pStatus) {

/* OMITTING CODE: same as Listing 1, Lines 20-30 */

/* Iterate over kernel's instructions */
for (auto &i: nvbit_get_instrs(ctx, p->func)) {
/* If instr isn't global memory operation skip */
if(i->getMemOpType() != Instr::GLOBAL) continue;
/* Iterate on operands of instruction */
for(int n = 0; n < i->getNumOperands(); n++) {
operand_t * op = i->getOperand(n);
/* If operand isn't a memory reference skip */
if(op->type != Instr::MREF) continue;
/* Inject instrumentation and its arguments */
 nvbit_insert_call(i, "ifunc", IPOINT_BEFORE);
 nvbit_add_call_arg(PRED_VAL);
nvbit_add_call_arg(REG_VAL, op->val[0]);
 nvbit_add_call_arg(REG_VAL, op->val[0] + 1);
 nvbit_add_call_arg(IMM32, op->val[1]);
 }}}

void nvbit_at_term() {
printf("Average cache lines requests per memory "
 "instruction %f\n", uniq_lines/mem_instrs);
 }

 
