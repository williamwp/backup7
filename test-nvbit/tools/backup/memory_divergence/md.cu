#include <assert.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>

/* every tool needs to include this once */
#include "nvbit_tool.h"

/* nvbit interface file */
#include "nvbit.h"
#include "cuda.h"
/* nvbit utility functions */
#include "utils/utils.h"
#include "utils/channel.hpp"

/* kernel id counter, maintained in system memory */
uint32_t kernel_id = 0;

/* global control variables for this tool */
uint32_t instr_begin_interval = 0;
uint32_t instr_end_interval = UINT32_MAX;
uint32_t ker_begin_interval = 0;
uint32_t ker_end_interval = UINT32_MAX;
int verbose = 0;
int count_warp_level = 1;
int exclude_pred_off = 0;




/* counters for unique cache lines accessed and for
the number of memory instructions executed */
__managed__ float uniq_lines = 0;
__managed__ long mem_instrs = 0;
extern "C" __device__ __noinline__
void ifunc(int pred, int r1, int r2, int imm) {
/* Return if predicate is false */
if(!pred) return;

 /* Construct address */
long addr = (((long)r1) | ((long)r2 << 32)) + imm;

/* Compute active mask of the warp */
int mask = __ballot(1);

/* Only the first active thread in the warp
increments the memory instruction counter */
if (get_laneid() == __ffs(mask) - 1)
(uint8_t *)atomicAdd(&mem_instrs, 1);

/* Count how many threads in the warp access
the same cache line */
//cache line size is 128, log2 of 128 is 7.
int LOG2_CACHE_LINE_SIZE = 7;
long cache_addr = addr >> LOG2_CACHE_LINE_SIZE;
int cnt = __popc(__match_any_sync(mask, cache_addr));

/* Each thread contributes proportionally to the
cache line counter */
(uint8_t *)atomicAdd(&uniq_lines, 1.0f / cnt);
} NVBIT_EXPORT_DEV_FUNC(ifunc);





/* a pthread mutex, used to prevent multiple kernels to run concurrently and
 * therefore to "corrupt" the counter variable */
pthread_mutex_t mutex;


/* nvbit_at_init() is executed as soon as the nvbit tool is loaded. We typically
 * do initializations in this call. In this case for instance we get some
 * environment variables values which we use as input arguments to the tool */
void nvbit_at_init() {
    /* just make sure all managed variables are allocated on GPU */
    setenv("CUDA_MANAGED_FORCE_DEVICE_ALLOC", "1", 1);

    /* we get some environment variables that are going to be use to selectively
     * instrument (within a interval of kernel indexes and instructions). By
     * default we instrument everything. */
    GET_VAR_INT(
        instr_begin_interval, "INSTR_BEGIN", 0,
        "Beginning of the instruction interval where to apply instrumentation");
    GET_VAR_INT(
        instr_end_interval, "INSTR_END", UINT32_MAX,
        "End of the instruction interval where to apply instrumentation");
    GET_VAR_INT(ker_begin_interval, "KERNEL_BEGIN", 0,
                "Beginning of the kernel launch interval where to apply "
                "instrumentation");
    GET_VAR_INT(
        ker_end_interval, "KERNEL_END", UINT32_MAX,
        "End of the kernel launch interval where to apply instrumentation");
    GET_VAR_INT(count_warp_level, "COUNT_WARP_LEVEL", 1,
                "Count warp level or thread level instructions");
    GET_VAR_INT(exclude_pred_off, "EXCLUDE_PRED_OFF", 0,
                "Exclude predicated off instruction from count");
    GET_VAR_INT(verbose, "TOOL_VERBOSE", 0, "Enable verbosity inside the tool");
    std::string pad(100, '-');
    printf("%s\n", pad.c_str());
}


void nvbit_at_cuda_event(CUcontext ctx, int is_exit, nvbit_api_cuda_t cbid,
                         const char *API_name, void *params, CUresult *pStatus) {
/* Return if not at the entry of a kernel launch */
 if (cbid != API_CUDA_cuLaunchKernel || is_exit)
 return;

 /* Get parameters of the kernel launch */
 cuLaunchKernel_params *p = (cuLaunchKernel_params *) params;

 /* Return if kernel is already instrumented */
 if(!instrumented_kernels.insert(p->func).second)
 return;
/* OMITTING CODE: same as Listing 1, Lines 20-30 */

/* Iterate over kernel's instructions */
for (auto &i: nvbit_get_instrs(ctx, p->func)) {
/* If instr isn't global memory operation skip */
if(i->getMemOpType() != Instr::GLOBAL) continue;
/* Iterate on operands of instruction */
for(int n = 0; n < i->getNumOperands(); n++) {
operand_t * op = i->getOperand(n);
/* If operand isn't a memory reference skip */
if(op->type != Instr::MREF) continue;
/* Inject instrumentation and its arguments */
 nvbit_insert_call(i, "ifunc", IPOINT_BEFORE);
 nvbit_add_call_arg(PRED_VAL);
nvbit_add_call_arg(REG_VAL, op->val[0]);
 nvbit_add_call_arg(REG_VAL, op->val[0] + 1); 
 nvbit_add_call_arg(IMM32, op->val[1]);
 }}


}

void nvbit_at_term() {
    printf("\nEND OF APPLICATION\n");
    printf("Average cache lines requests per memory "
    "instruction %f\n", uniq_lines/mem_instrs);
}

