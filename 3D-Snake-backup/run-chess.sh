#!/bin/bash

start_time=$(date +%s)
sleep 1

#./snake

# name run-snake.sh

#echo $$ > /home/wangpeng/tools/3d/80/3D-Snake/run-snake.log
#sleep 5 && kill 'cat /home/wangpeng/tools/3d/80/3D-Snake/run-snake.log' &

#start code
echo "International Chess"
#end code

sleep 2
xdotool mousemove 2126 627 click 1 
xdotool mousemove 2123 489 click 1 
echo "white pawn moves"

sleep 2
xdotool mousemove 2121 322 click 1
xdotool mousemove 2124 430 click 1
echo "black pawn moves"

sleep 2
xdotool mousemove 2048 700 click 1
xdotool mousemove 2330 428 click 1
echo "white queen takes sideways"

sleep 2
xdotool mousemove 2240 270 click 1
xdotool mousemove 2188 377 click 1
echo "black knight leaps"

sleep 2
xdotool mousemove 2327 431 click 1
xdotool mousemove 2121 429 click 1
echo "white queen kills black pawn"

sleep 2
xdotool mousemove 1923 275 click 1
xdotool mousemove 1992 375 click 1
echo "black pawn moves"

sleep 2
echo "white queen moves sideways"
xdotool mousemove 2123 436 click 1
xdotool mousemove 2259 430 click 1

sleep 2
echo "black pawn moves"
xdotool mousemove 2253 321 click 1
xdotool mousemove 2249 372 click 1

sleep 2
echo "white queen walks sideways"
xdotool mousemove 2258 431 click 1
xdotool mousemove 2126 558 click 1

sleep 2
echo "black pawn moves"
xdotool mousemove 2310 321 click 1
xdotool mousemove 2325 433 click 1

sleep 2
echo "white knight leaps"
xdotool mousemove 1887 697 click 1
xdotool mousemove 1834 559 click 1

sleep 2
echo "black rock moves"
xdotool mousemove 1871 275 click 1
xdotool mousemove 1932 274 click 1

sleep 5
echo "white pawn moves"
xdotool mousemove 1900 631 click 1
xdotool mousemove 1904 559 click 1

sleep 5
echo "black pawn moves"
xdotool mousemove 1933 326 click 1
xdotool mousemove 1922 434 click 1

sleep 2
echo "white bishop kills black pawn"
xdotool mousemove 2207 703 click 1
xdotool mousemove 1920 435 click 1

sleep 2
echo "black rock kills white bishop"
xdotool mousemove 1932 273 click 1
xdotool mousemove 1919 430 click 1

sleep 2
echo "white knight kills black rock"
xdotool mousemove 1834 559 click 1
xdotool mousemove 1917 434 click 1

sleep 2
echo "black pwan moves"
xdotool mousemove 1867 323 click 1
xdotool mousemove 1859 377 click 1

sleep 2
echo "white knoght leaps"
xdotool mousemove 1919 439 click 1
xdotool mousemove 2050 493 click 1

sleep 2
echo "black pawn moves"
xdotool mousemove 2054 323 click 1
xdotool mousemove 2055 429 click 1

sleep 2
echo "white pawn kills black pawn"
xdotool mousemove 2124 481 click 1
xdotool mousemove 2052 429 click 1
echo "--------------------------------------------------"
echo "warning ! check !"
echo ""
# the rules in the international chess 

sleep 4
echo "black bishop moves"
xdotool mousemove 2184 274 click 1
xdotool mousemove 2120 322 click 1

sleep 2
echo "white bishop moves"
xdotool mousemove 1969 700 click 1
xdotool mousemove 1836 563 click 1

sleep 2
echo "black knight kills white pawn"
xdotool mousemove 2191 379 click 1
xdotool mousemove 2053 429 click 1

sleep 2
echo "white bishop kills black bishop"
xdotool mousemove 1835 560 click 1
xdotool mousemove 2121 325 click 1

sleep 2
echo "black queen kills white bishop"
xdotool mousemove 2060 273 click 1
xdotool mousemove 2122 328 click 1
echo ""

sleep 2
echo "white queen kills black queen"
echo "warning ! check !"
xdotool mousemove 2125 548 click 1
xdotool mousemove 2119 326 click 1
echo "--------------------------------------------------"
echo "congratulations ! white win !"
echo "--------------------------------------------------"
# the rules in the international chess

end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
