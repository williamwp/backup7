#!/bin/bash

akey=(0 0 0)

cESC=`echo -ne "\033"`

while :
do
      read -s -n 1 key

      akey[0]=${akey[1]}
      akey[1]=${akey[2]}
      akey[2]=${key}

      if [[ ${key} == ${cESC} && ${akey[1]} == ${cESC} ]]
      then
         echo "ESC key"

      elif [[ ${akey[0]} == ${cESC} && ${akey[1]} == "[" ]]
      then
           if [[ ${key} == "A" ]];
	       then echo "up"
           elif [[ ${key} == "B" ]];
	       then echo "down"
           elif [[ ${key} == "D" ]];
	       then echo "left"
           elif [[ ${key} == "C" ]];
	       then echo "right"
           fi
      fi
done
