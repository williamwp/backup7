wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ gcc --version
gcc (Ubuntu 9.4.0-1ubuntu1~18.04) 9.4.0
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ ls /usr/bin/gcc*
/usr/bin/gcc    /usr/bin/gcc-9     /usr/bin/gcc-ar-7    /usr/bin/gcc-nm    /usr/bin/gcc-nm-9      /usr/bin/gcc-ranlib-7
/usr/bin/gcc-6  /usr/bin/gcc-ar    /usr/bin/gcc-ar-9    /usr/bin/gcc-nm-6  /usr/bin/gcc-ranlib    /usr/bin/gcc-ranlib-9
/usr/bin/gcc-7  /usr/bin/gcc-ar-6  /usr/bin/gccmakedep  /usr/bin/gcc-nm-7  /usr/bin/gcc-ranlib-6
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 40  --slave /usr/bin/g++ g++ /usr/bin/g++-6
[sudo] password for wangpeng: 
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 50  --slave /usr/bin/g++ g++ /usr/bin/g++-7
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 70  --slave /usr/bin/g++ g++ /usr/bin/g++-9
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ sudo update-alternatives --config gcc
There are 3 choices for the alternative gcc (providing /usr/bin/gcc).

  Selection    Path            Priority   Status
------------------------------------------------------------
* 0            /usr/bin/gcc-9   70        auto mode
  1            /usr/bin/gcc-6   40        manual mode
  2            /usr/bin/gcc-7   50        manual mode
  3            /usr/bin/gcc-9   70        manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
update-alternatives: using /usr/bin/gcc-7 to provide /usr/bin/gcc (gcc) in manual mode
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ gcc --version
gcc (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/88-Metal-reflection-sample3$ 

