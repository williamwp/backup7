==31999== Profiling application: ./a.out
==31999== Profiling result:
==31999== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: render_kernel(int, int, int, Polygon*, float3*, float3*, float3, float3)
         80                             inst_per_warp                                                 Instructions per warp  1.2031e+05  1.2039e+05  1.2032e+05
         80                         branch_efficiency                                                     Branch Efficiency      87.97%      87.97%      87.97%
         80                 warp_execution_efficiency                                             Warp Execution Efficiency      72.94%      72.99%      72.98%
         80         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      70.07%      70.12%      70.11%
         80                      inst_replay_overhead                                           Instruction Replay Overhead    0.000028    0.000029    0.000029
         80      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         80     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         80       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         80      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         80              gld_transactions_per_request                                  Global Load Transactions Per Request   15.088964   15.088964   15.088964
         80              gst_transactions_per_request                                 Global Store Transactions Per Request   16.000000   16.000000   16.000000
         80                 shared_store_transactions                                             Shared Store Transactions           0           0           0
         80                  shared_load_transactions                                              Shared Load Transactions           0           0           0
         80                   local_load_transactions                                               Local Load Transactions           0           0           0
         80                  local_store_transactions                                              Local Store Transactions           0           0           0
         80                          gld_transactions                                              Global Load Transactions  4821872642  4821872642  4821872642
         80                          gst_transactions                                             Global Store Transactions     2396160     2396160     2396160
         80                  sysmem_read_transactions                                       System Memory Read Transactions           0         213          72
         80                 sysmem_write_transactions                                      System Memory Write Transactions           5          37           7
         80                      l2_read_transactions                                                  L2 Read Transactions     1007738     1379163     1116881
         80                     l2_write_transactions                                                 L2 Write Transactions     2396208     2592359     2436230
         80                    dram_read_transactions                                       Device Memory Read Transactions      300187      486932      326711
         80                   dram_write_transactions                                      Device Memory Write Transactions      563914      766932      626120
         80                           global_hit_rate                                     Global Hit Rate in unified l1/tex      99.72%      99.72%      99.72%
         80                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
         80                  gld_requested_throughput                                      Requested Global Load Throughput  54.581GB/s  54.680GB/s  54.671GB/s
         80                  gst_requested_throughput                                     Requested Global Store Throughput  772.49MB/s  773.92MB/s  773.81MB/s
         80                            gld_throughput                                                Global Load Throughput  456.42GB/s  457.25GB/s  457.17GB/s
         80                            gst_throughput                                               Global Store Throughput  3.0175GB/s  3.0231GB/s  3.0227GB/s
         80                     local_memory_overhead                                                 Local Memory Overhead       0.00%       0.00%       0.00%
         80                        tex_cache_hit_rate                                                Unified Cache Hit Rate      99.75%      99.75%      99.75%
         80                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      70.11%      70.35%      70.20%
         80                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      91.67%      91.67%      91.67%
         80                      dram_read_throughput                                         Device Memory Read Throughput  387.78MB/s  629.02MB/s  422.03MB/s
         80                     dram_write_throughput                                        Device Memory Write Throughput  728.46MB/s  990.73MB/s  808.79MB/s
         80                      tex_cache_throughput                                              Unified Cache Throughput  1609.7GB/s  1612.7GB/s  1612.5GB/s
         80                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  1.2655GB/s  1.2700GB/s  1.2688GB/s
         80                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  3.0175GB/s  3.0231GB/s  3.0227GB/s
         80                        l2_read_throughput                                                 L2 Throughput (Reads)  1.2707GB/s  1.7398GB/s  1.4089GB/s
         80                       l2_write_throughput                                                L2 Throughput (Writes)  3.0177GB/s  3.2705GB/s  3.0733GB/s
         80                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  281.76KB/s  95.948KB/s
         80                   sysmem_write_throughput                                        System Memory Write Throughput  6.6113KB/s  48.943KB/s  10.350KB/s
         80                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         80                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         80                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         80                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         80                            gld_efficiency                                         Global Memory Load Efficiency      11.96%      11.96%      11.96%
         80                            gst_efficiency                                        Global Memory Store Efficiency      25.00%      25.00%      25.00%
         80                    tex_cache_transactions                                            Unified Cache Transactions  1278251520  1278251520  1278251520
         80                             flop_count_dp                           Floating Point Operations(Double Precision)    71884800    71884800    71884800
         80                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)    14376960    14376960    14376960
         80                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)    28753920    28753920    28753920
         80                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)           0           0           0
         80                             flop_count_sp                           Floating Point Operations(Single Precision)  4.4954e+10  4.4960e+10  4.4957e+10
         80                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)  9621161195  9622580812  9621811951
         80                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  1.4868e+10  1.4870e+10  1.4869e+10
         80                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)  5597065519  5598016269  5597508630
         80                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   904920434   905123024   905100185
         80                             inst_executed                                                 Instructions Executed  3003044188  3005038801  3003165414
         80                               inst_issued                                                   Instructions Issued  3003128831  3005125935  3003251040
         80                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
         80                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
         80                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)      13.86%      13.87%      13.86%
         80                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      13.84%      13.84%      13.84%
         80                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       4.30%       4.33%       4.32%
         80                             stall_texture                                         Issue Stall Reasons (Texture)       0.87%       0.88%       0.88%
         80                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
         80                               stall_other                                           Issue Stall Reasons (Other)      60.43%      60.44%      60.43%
         80          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.01%       0.02%       0.01%
         80                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       0.58%       0.58%       0.58%
         80                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
         80                                inst_fp_32                                               FP Instructions(Single)  3.5406e+10  3.5411e+10  3.5408e+10
         80                                inst_fp_64                                               FP Instructions(Double)    57507840    57507840    57507840
         80                              inst_integer                                                  Integer Instructions  1.0636e+10  1.0638e+10  1.0636e+10
         80                          inst_bit_convert                                              Bit-Convert Instructions   134184960   134184960   134184960
         80                              inst_control                                             Control-Flow Instructions  4719669143  4720666722  4719739178
         80                        inst_compute_ld_st                                               Load/Store Instructions  1.0231e+10  1.0231e+10  1.0231e+10
         80                                 inst_misc                                                     Misc Instructions  6192191724  6193086284  6192355496
         80           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
         80                               issue_slots                                                           Issue Slots  2750525636  2752252515  2750636227
         80                                 cf_issued                                      Issued Control-Flow Instructions   339355643   339955469   339376854
         80                               cf_executed                                    Executed Control-Flow Instructions   339355643   339955469   339376854
         80                               ldst_issued                                        Issued Load/Store Instructions  1278950400  1278950400  1278950400
         80                             ldst_executed                                      Executed Load/Store Instructions   319812480   319812480   319812480
         80                       atomic_transactions                                                   Atomic Transactions           0           0           0
         80           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
         80                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
         80                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
         80                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)     1004814     1006685     1005771
         80                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
         80                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       6.07%       6.08%       6.08%
         80                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)     2396160     2396160     2396160
         80                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
         80                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
         80                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
         80                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
         80                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
         80                                       ipc                                                          Executed IPC    1.718104    1.719028    1.718817
         80                                issued_ipc                                                            Issued IPC    1.718154    1.719077    1.718870
         80                    issue_slot_utilization                                                Issue Slot Utilization      78.68%      78.72%      78.71%
         80                             sm_efficiency                                               Multiprocessor Activity      99.35%      99.39%      99.37%
         80                        achieved_occupancy                                                    Achieved Occupancy    0.442488    0.442589    0.442544
         80                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    3.190837    3.193787    3.193203
         80                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
         80                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
         80                           tex_utilization                                             Unified Cache Utilization    High (8)    High (8)    High (8)
         80                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
         80                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
         80                        tex_fu_utilization                                     Texture Function Unit Utilization    High (8)    High (8)    High (8)
         80                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
         80             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
         80           single_precision_fu_utilization                            Single-Precision Function Unit Utilization    Max (10)    Max (10)    Max (10)
         80           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
         80                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
         80                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)      19.95%      19.98%      19.98%
         80                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.06%       0.06%       0.06%
         80                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)     Low (1)    Idle (0)
         80                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
         80               pcie_total_data_transmitted                                           PCIe Total Data Transmitted        5632    13182976     1561107
         80                  pcie_total_data_received                                              PCIe Total Data Received           0     2043904      772384
         80                inst_executed_global_loads                              Warp level instructions for global loads   319562880   319562880   319562880
         80                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
         80                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
         80               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
         80               inst_executed_global_stores                             Warp level instructions for global stores      149760      149760      149760
         80                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
         80               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
         80              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
         80              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
         80           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
         80             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
         80          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
         80              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
         80                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
         80                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads    32159840    32212960    32183294
         80                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
         80                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
         80                           dram_read_bytes                                Total bytes read from DRAM to L2 cache     9605984    15581824    10454763
         80                          dram_write_bytes                             Total bytes written from L2 cache to DRAM    18045248    24541824    20035850
         80               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.    76677120    76677120    76677120
         80                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
         80              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
         80                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
         80                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
         80             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
         80                      global_load_requests              Total number of global load requests from Multiprocessor  1278251520  1278251520  1278251520
         80                       local_load_requests               Total number of local load requests from Multiprocessor           0           0           0
         80                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
         80                     global_store_requests             Total number of global store requests from Multiprocessor      599040      599040      599040
         80                      local_store_requests              Total number of local store requests from Multiprocessor           0           0           0
         80                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
         80                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
         80                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
         80                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
         80                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
         80                         sysmem_read_bytes                                              System Memory Read Bytes           0        6816        2321
         80                        sysmem_write_bytes                                             System Memory Write Bytes         160        1184         250
         80                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      85.29%      85.36%      85.32%
         80                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
         80                     unique_warps_launched                                              Number of warps launched       24960       24960       24960
wangpeng@dacent:~/tools/gitlab-william/backup7/nbody+lite-experiment/13-LiteTracer-sample1$
