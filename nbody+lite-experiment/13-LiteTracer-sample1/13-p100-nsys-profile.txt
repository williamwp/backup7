
Importing [==================================================100%]
Saving report to file "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/13-LiteTracer-sample1/report2.qdrep"
Report file saved.
Please discard the qdstrm file and use the qdrep file instead.

Removed /home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/13-LiteTracer-sample1/report2.qdstrm as it was successfully imported.
Please use the qdrep file instead.

Exporting the qdrep file to SQLite database using /usr/local/cuda-10.2/nsight-systems-2019.5.2/host-linux-x64/nsys-exporter.

Exporting 6291694 events:

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/13-LiteTracer-sample1/report2.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   96.6  63511569398244      866675      73281875.4            2528   2000810802419  pthread_cond_wait                                                               
    3.1   2015612822478      288003       6998582.7            1058       100248484  poll                                                                            
    0.2    126262563740     1608532         78495.5            1003         1822567  pthread_barrier_wait                                                            
    0.1     95673030784     2230055         42901.6            1000         1739028  pthread_mutex_lock                                                              
    0.0     22520845943      251205         89651.3            8209          399552  writev                                                                          
    0.0      3882608536      866728          4479.6            1000           77429  pthread_cond_signal                                                             
    0.0       166370372         613        271403.5            1035        79520346  ioctl                                                                           
    0.0       143488168       44705          3209.7            1000           27688  recvmsg                                                                         
    0.0        16351651          29        563850.0            7109         4560996  pthread_join                                                                    
    0.0         3075006          63         48809.6            1725         1279101  mmap                                                                            
    0.0         2215395          11        201399.5            1130         1690174  fread                                                                           
    0.0         1725791          34         50758.6           27131          135224  pthread_create                                                                  
    0.0         1566038          32         48938.7            2900         1365654  fopen                                                                           
    0.0         1338410           5        267682.0            4604         1297826  open                                                                            
    0.0         1316915          65         20260.2            1965           50752  open64                                                                          
    0.0          902306          43         20983.9            1049           66638  read                                                                            
    0.0          826848          59         14014.4            1040           90043  pthread_cond_broadcast                                                          
    0.0          368400           9         40933.3           17513           83393  sem_timedwait                                                                   
    0.0          320600           3        106866.7           81976          137187  fgets                                                                           
    0.0          275634          30          9187.8            1627           35013  recv                                                                            
    0.0           65830           2         32915.0           13240           52590  connect                                                                         
    0.0           65525          26          2520.2            1431            5240  fclose                                                                          
    0.0           55902          12          4658.5            2084           13305  munmap                                                                          
    0.0           46340          16          2896.3            1724            3980  prctl                                                                           
    0.0           33466          10          3346.6            1992            5486  write                                                                           
    0.0           24060           1         24060.0           24060           24060  shutdown                                                                        
    0.0           22625           3          7541.7            5402           11377  socket                                                                          
    0.0           22500           1         22500.0           22500           22500  shmget                                                                          
    0.0           19451          10          1945.1            1029            4991  fcntl                                                                           
    0.0           17127           6          2854.5            1806            5352  mprotect                                                                        
    0.0            9306           1          9306.0            9306            9306  pipe2                                                                           
    0.0            8029           1          8029.0            8029            8029  shmat                                                                           
    0.0            7073           2          3536.5            3293            3780  mmap64                                                                          
    0.0            4849           2          2424.5            1136            3713  fgets_unlocked                                                                  
    0.0            2280           1          2280.0            2280            2280  bind                                                                            
    0.0            1962           1          1962.0            1962            1962  pthread_mutex_trylock                                                           
    0.0            1671           1          1671.0            1671            1671  shmctl                                                                          
    0.0            1282           1          1282.0            1282            1282  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)


