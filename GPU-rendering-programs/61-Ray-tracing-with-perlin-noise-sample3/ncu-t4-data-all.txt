==PROF== Connected to process 32499 (/home/wangpeng/tools/cloud-rendering/5c/main)
==PROF== Profiling "populate_scene_balls" - 1: Loaded image with 1200x600 and 3 channels
Rendering a 1200x600 image (100 samples per pixel) in 4x4 blocks.
0%....50%....100% - 14 passes
==PROF== Profiling "init_rand_state" - 2: 0%....50%....100% - 14 passes
==PROF== Profiling "render" - 3: 0%....50%....100% - 14 passes
took 89251421us.
==PROF== Profiling "free_scene" - 4: Deleting sphere object at 0x7fa1cabfacc0
    ---------------------------------------------------------------------- --------------- ------------------------------

  render(vec3 *, int, int, hitable_list **, camera **, curandStateXORWOW *), 2022-May-07 12:22:32, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Gbyte                         161.38
    dram__bytes_read.sum.per_second                                           Gbyte/second                          65.75
    dram__bytes_write.sum                                                            Gbyte                         215.26
    dram__bytes_write.sum.per_second                                          Gbyte/second                          87.71
    dram__sectors_read.sum                                                          sector                  5,043,004,225
    dram__sectors_write.sum                                                         sector                  6,727,017,208
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          13.50
    l1tex__lsu_writeback_active.sum                                                  cycle                  7,724,172,948
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          42.23
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                          34.56
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                          14.08
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                         112.40
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                  2,565,334,457
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                        135,000
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                  5,043,106,018
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                  4,490,233,282
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          57.67
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                  3,239,203,576
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                  8,620,878,551
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                  7,688,442,359
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/nsecond                           3.13
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Gbyte                           4.89
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                          11.64
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          80.62
    lts__t_sector_op_read_hit_rate.pct                                                   %                          78.43
    lts__t_sector_op_write_hit_rate.pct                                                  %                          82.69
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                          22.27
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                            154
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                   sector/second                          62.75
    lts__t_sectors_op_read.sum                                                      sector                  7,288,011,284
    lts__t_sectors_op_read.sum.per_second                                   sector/nsecond                           2.97
    lts__t_sectors_op_write.sum                                                     sector                  7,685,283,103
    lts__t_sectors_op_write.sum.per_second                                  sector/nsecond                           3.13
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                  4,147,250,208
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/nsecond                           1.69
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           7.47
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          49.94
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                   1,326,436.32
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          99.55
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.26
    smsp__inst_executed.sum                                                           inst                 60,287,856,964
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                     18,632,431
    smsp__inst_executed_op_global_st.sum                                              inst                        135,000
    smsp__inst_executed_op_local_ld.sum                                               inst                  3,359,963,906
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                    336,631,140
    smsp__inst_executed_pipe_cbu.sum                                                  inst                  8,315,056,910
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                           2.19
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           9.66
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.26
    smsp__inst_issued.sum                                                             inst                 60,569,759,772
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          26.45
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                 58,980,091,567
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                    345,406,025
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                  1,126,733,840
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                    225,346,846
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                 28,723,820,343
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                 29,357,994,667
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                 20,358,533,497
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                117,395,608,807
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                  1,697,486,789
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                158,482,458,781
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                 99,154,982,427
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                 49,556,782,778
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          26.11
    smsp__thread_inst_executed_per_inst_executed.ratio                                                               8.35
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.00
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                          53.24
    ---------------------------------------------------------------------- --------------- ------------------------------

  free_scene(hitable_object **, hitable_list **, camera **), 2022-May-07 12:22:33, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Kbyte                          97.98
    dram__bytes_read.sum.per_second                                           Mbyte/second                          61.93
    dram__bytes_write.sum                                                             byte                            896
    dram__bytes_write.sum.per_second                                          Kbyte/second                         566.31
    dram__sectors_read.sum                                                          sector                          3,062
    dram__sectors_write.sum                                                         sector                             28
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           1.19
    l1tex__lsu_writeback_active.sum                                                  cycle                         10,917
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Mbyte/second                          43.20
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         112.19
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                          70.91
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Mbyte/second                         167.20
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                            206
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                          2,136
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                             18
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                          3,506
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                          8,267
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                          2,405
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          84.28
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                          2,136
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                          8,267
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                          2,405
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/usecond                           1.52
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                             206
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                               18
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                          68.13
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Kbyte                           3.26
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          91.16
    lts__t_sector_op_read_hit_rate.pct                                                   %                          44.25
    lts__t_sector_op_write_hit_rate.pct                                                  %                          97.81
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           0.07
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                             16
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                   sector/msecond                          10.11
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                          3,463
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/usecond                           2.19
    lts__t_sectors_op_read.sum                                                      sector                         11,810
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                           7.46
    lts__t_sectors_op_write.sum                                                     sector                          5,939
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                           3.75
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                          2,161
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                           1.37
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           0.01
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                        104,731
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                           0.63
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.11
    smsp__inst_executed.sum                                                           inst                        104,731
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                             20
    smsp__inst_executed_op_global_st.sum                                              inst                             48
    smsp__inst_executed_op_local_ld.sum                                               inst                          8,053
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                             81
    smsp__inst_executed_pipe_cbu.sum                                                  inst                          4,243
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           4.31
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                           0.01
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.11
    smsp__inst_issued.sum                                                             inst                        104,863
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          11.35
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                             96
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                            232
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                            234
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                            150
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                           3.12
    smsp__thread_inst_executed_per_inst_executed.ratio                                                                  1
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.12
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                          18.16
    ---------------------------------------------------------------------- --------------- ------------------------------

