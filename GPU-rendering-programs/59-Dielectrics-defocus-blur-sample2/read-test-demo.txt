145 int main() {
146     int nx = 2560;
147     int ny = 1440;
148     int ns = 100;
149     int tx = 32;
150     int ty = 32;
151 
152     std::cerr << "Rendering a " << nx << "x" << ny << " image with " << ns << " samples per pixel ";
153     std::cerr << "in " << tx << "x" << ty << " blocks.\n";

