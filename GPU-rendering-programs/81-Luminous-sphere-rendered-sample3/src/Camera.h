#ifndef RAYTRACING_CAMERA_H
#define RAYTRACING_CAMERA_H

#include "/home/wangpeng/tools/gitlab-william/backup7/GPU-rendering-programs/81-Luminous-sphere-rendered-sample3/src/glm/vec3.hpp"

struct Camera {
    glm::vec3 origin;
    glm::vec3 direction;
};

#endif //RAYTRACING_CAMERA_H
