#ifndef RAYTRACING_PNG_H
#define RAYTRACING_PNG_H

#include <string>
#include "/home/wangpeng/tools/gitlab-william/backup7/GPU-rendering-programs/81-Luminous-sphere-rendered-sample3/src/glm/vec3.hpp"
#include <vector>

namespace PNG {

    void WriteImage(const std::string& path, int width, int height, const std::vector<glm::vec3>& image);

}

#endif //RAYTRACING_PNG_H
