#ifndef RAYTRACING_DISPLAY_H
#define RAYTRACING_DISPLAY_H

#include <iostream>
#include <vector>
#include "/home/wangpeng/tools/gitlab-william/backup7/GPU-rendering-programs/81-Luminous-sphere-rendered-sample3/src/glm/vec3.hpp"
#include "utils/cuda_memory.h"

class Display {
public:
    Display(int, int);

    CUDA::device_ptr<glm::vec3> GetDisplay();

    std::vector<glm::vec3> GetImage() const;
private:
    CUDA::unique_ptr<glm::vec3> display_;
    int width_;
    int height_;
};


#endif //RAYTRACING_DISPLAY_H
