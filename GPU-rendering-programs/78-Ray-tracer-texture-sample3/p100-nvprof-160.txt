==3588== Profiling application: ./a.out
==3588== Profiling result:
==3588== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: render(RGBType*, int, int, double, int, int, double, double, double, int, sphere*, int, plane*, int, light*, int, camera)
          1                             inst_per_warp                                                 Instructions per warp  2.3320e+04  2.3320e+04  2.3320e+04
          1                         branch_efficiency                                                     Branch Efficiency      99.98%      99.98%      99.98%
          1                 warp_execution_efficiency                                             Warp Execution Efficiency      98.00%      98.00%      98.00%
          1         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      92.18%      92.18%      92.18%
          1                      inst_replay_overhead                                           Instruction Replay Overhead    0.000071    0.000071    0.000071
          1      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
          1     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
          1       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    7.930679    7.930679    7.930679
          1      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    3.979868    3.979868    3.979868
          1              gld_transactions_per_request                                  Global Load Transactions Per Request   31.823988   31.823988   31.823988
          1              gst_transactions_per_request                                 Global Store Transactions Per Request   23.999816   23.999816   23.999816
          1                 shared_store_transactions                                             Shared Store Transactions           0           0           0
          1                  shared_load_transactions                                              Shared Load Transactions           0           0           0
          1                   local_load_transactions                                               Local Load Transactions   664566052   664566052   664566052
          1                  local_store_transactions                                              Local Store Transactions   332687962   332687962   332687962
          1                          gld_transactions                                              Global Load Transactions   629951618   629951618   629951618
          1                          gst_transactions                                             Global Store Transactions     4704444     4704444     4704444
          1                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
          1                 sysmem_write_transactions                                      System Memory Write Transactions           5           5           5
          1                      l2_read_transactions                                                  L2 Read Transactions   333780096   333780096   333780096
          1                     l2_write_transactions                                                 L2 Write Transactions   337434282   337434282   337434282
          1                    dram_read_transactions                                       Device Memory Read Transactions      213599      213599      213599
          1                   dram_write_transactions                                      Device Memory Write Transactions    10771210    10771210    10771210
          1                           global_hit_rate                                     Global Hit Rate in unified l1/tex      96.81%      96.81%      96.81%
          1                            local_hit_rate                                                        Local Hit Rate      49.87%      49.87%      49.87%
          1                  gld_requested_throughput                                      Requested Global Load Throughput  6.4252GB/s  6.4252GB/s  6.4252GB/s
          1                  gst_requested_throughput                                     Requested Global Store Throughput  2.0360GB/s  2.0360GB/s  2.0360GB/s
          1                            gld_throughput                                                Global Load Throughput  25.701GB/s  25.701GB/s  25.701GB/s
          1                            gst_throughput                                               Global Store Throughput  6.1081GB/s  6.1081GB/s  6.1081GB/s
          1                     local_memory_overhead                                                 Local Memory Overhead      99.20%      99.20%      99.20%
          1                        tex_cache_hit_rate                                                Unified Cache Hit Rate      52.40%      52.40%      52.40%
          1                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      99.94%      99.94%      99.94%
          1                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      99.26%      99.26%      99.26%
          1                      dram_read_throughput                                         Device Memory Read Throughput  283.99MB/s  283.99MB/s  283.99MB/s
          1                     dram_write_throughput                                        Device Memory Write Throughput  13.985GB/s  13.985GB/s  13.985GB/s
          1                      tex_cache_throughput                                              Unified Cache Throughput  639.48GB/s  639.48GB/s  639.48GB/s
          1                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  433.37GB/s  433.37GB/s  433.37GB/s
          1                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  438.06GB/s  438.06GB/s  438.06GB/s
          1                        l2_read_throughput                                                 L2 Throughput (Reads)  433.37GB/s  433.37GB/s  433.37GB/s
          1                       l2_write_throughput                                                L2 Throughput (Writes)  438.11GB/s  438.11GB/s  438.11GB/s
          1                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   sysmem_write_throughput                                        System Memory Write Throughput  6.8066KB/s  6.8066KB/s  6.8057KB/s
          1                     local_load_throughput                                          Local Memory Load Throughput  862.85GB/s  862.85GB/s  862.85GB/s
          1                    local_store_throughput                                         Local Memory Store Throughput  431.95GB/s  431.95GB/s  431.95GB/s
          1                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                            gld_efficiency                                         Global Memory Load Efficiency      25.00%      25.00%      25.00%
          1                            gst_efficiency                                        Global Memory Store Efficiency      33.33%      33.33%      33.33%
          1                    tex_cache_transactions                                            Unified Cache Transactions   492523343   492523343   492523343
          1                             flop_count_dp                           Floating Point Operations(Double Precision)  3.2287e+10  3.2287e+10  3.2287e+10
          1                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)  7956011434  7956011434  7956011434
          1                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)  1.0983e+10  1.0983e+10  1.0983e+10
          1                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)  2365657526  2365657526  2365657526
          1                             flop_count_sp                           Floating Point Operations(Single Precision)   401041436   401041436   401041436
          1                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)           0           0           0
          1                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)   200520718   200520718   200520718
          1                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)           0           0           0
          1                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   482568204   482568204   482568204
          1                             inst_executed                                                 Instructions Executed  1535047061  1535047061  1535047061
          1                               inst_issued                                                   Instructions Issued  1535157670  1535157670  1535157670
          1                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
          1                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
          1                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)       9.16%       9.16%       9.16%
          1                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      47.59%      47.59%      47.59%
          1                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)      28.17%      28.17%      28.17%
          1                             stall_texture                                         Issue Stall Reasons (Texture)       2.09%       2.09%       2.09%
          1                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
          1                               stall_other                                           Issue Stall Reasons (Other)       2.91%       2.91%       2.91%
          1          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.03%       0.03%       0.03%
          1                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       5.90%       5.90%       5.90%
          1                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
          1                                inst_fp_32                                               FP Instructions(Single)  1311235740  1311235740  1311235740
          1                                inst_fp_64                                               FP Instructions(Double)  2.2365e+10  2.2365e+10  2.2365e+10
          1                              inst_integer                                                  Integer Instructions  6811505556  6811505556  6811505556
          1                          inst_bit_convert                                              Bit-Convert Instructions   144855491   144855491   144855491
          1                              inst_control                                             Control-Flow Instructions  2369322068  2369322068  2369322068
          1                        inst_compute_ld_st                                               Load/Store Instructions  5871452752  5871452752  5871452752
          1                                 inst_misc                                                     Misc Instructions  6405826003  6405826003  6405826003
          1           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
          1                               issue_slots                                                           Issue Slots  1410249181  1410249181  1410249181
          1                                 cf_issued                                      Issued Control-Flow Instructions    87246229    87246229    87246229
          1                               cf_executed                                    Executed Control-Flow Instructions    87246229    87246229    87246229
          1                               ldst_issued                                        Issued Load/Store Instructions   749597113   749597113   749597113
          1                             ldst_executed                                      Executed Load/Store Instructions   187905127   187905127   187905127
          1                       atomic_transactions                                                   Atomic Transactions           0           0           0
          1           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
          1                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
          1                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
          1                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)   333780640   333780640   333780640
          1                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.24%       0.24%       0.24%
          1                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       3.91%       3.91%       3.91%
          1                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)   337392406   337392406   337392406
          1                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
          1                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
          1                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
          1                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
          1                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
          1                                       ipc                                                          Executed IPC    0.908997    0.908997    0.908997
          1                                issued_ipc                                                            Issued IPC    0.909794    0.909794    0.909794
          1                    issue_slot_utilization                                                Issue Slot Utilization      41.79%      41.79%      41.79%
          1                             sm_efficiency                                               Multiprocessor Activity      99.75%      99.75%      99.75%
          1                        achieved_occupancy                                                    Achieved Occupancy    0.122463    0.122463    0.122463
          1                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    1.109452    1.109452    1.109452
          1                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
          1                            l2_utilization                                                  L2 Cache Utilization     Mid (4)     Mid (4)     Mid (4)
          1                           tex_utilization                                             Unified Cache Utilization     Low (3)     Low (3)     Low (3)
          1                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                        tex_fu_utilization                                     Texture Function Unit Utilization     Mid (5)     Mid (5)     Mid (5)
          1                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
          1           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Mid (5)     Mid (5)     Mid (5)
          1                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
          1                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       0.19%       0.19%       0.19%
          1                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)      29.80%      29.80%      29.80%
          1                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
          1                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
          1               pcie_total_data_transmitted                                           PCIe Total Data Transmitted       11776       11776       11776
          1                  pcie_total_data_received                                              PCIe Total Data Received           0           0           0
          1                inst_executed_global_loads                              Warp level instructions for global loads    19794867    19794867    19794867
          1                 inst_executed_local_loads                               Warp level instructions for local loads    83796870    83796870    83796870
          1                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
          1               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
          1               inst_executed_global_stores                             Warp level instructions for global stores      196020      196020      196020
          1                inst_executed_local_stores                              Warp level instructions for local stores    83592714    83592714    83592714
          1               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
          1              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
          1              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
          1           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
          1             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
          1          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
          1              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
          1                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
          1                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads    20222304    20222304    20222304
          1                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads  1.0661e+10  1.0661e+10  1.0661e+10
          1                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
          1                           dram_read_bytes                                Total bytes read from DRAM to L2 cache     6835168     6835168     6835168
          1                          dram_write_bytes                             Total bytes written from L2 cache to DRAM   344678720   344678720   344678720
          1               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.  1.0797e+10  1.0797e+10  1.0797e+10
          1                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
          1              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
          1                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
          1                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
          1             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
          1                      global_load_requests              Total number of global load requests from Multiprocessor    78743952    78743952    78743952
          1                       local_load_requests               Total number of local load requests from Multiprocessor   335035439   335035439   335035439
          1                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
          1                     global_store_requests             Total number of global store requests from Multiprocessor     1568148     1568148     1568148
          1                      local_store_requests              Total number of local store requests from Multiprocessor   333724918   333724918   333724918
          1                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
          1                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
          1                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
          1                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
          1                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
          1                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
          1                        sysmem_write_bytes                                             System Memory Write Bytes         160         160         160
          1                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      99.60%      99.60%      99.60%
          1                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
          1                     unique_warps_launched                                              Number of warps launched       65824       65824       65824
wangpeng@dacent:~/tools/gitlab-william/backup7/GPU-rendering-programs/78-Ray-tracer-texture-sample3$ 
